from fastapi import FastAPI, HTTPException, Depends, Header
from fastapi.responses import JSONResponse
import requests
from jose import JWTError, jwt

app_events = FastAPI()

# Placeholder until the events db is ready

users_server_url = "http://usersip:5003" 
data_server_url = "http://dataip:5002" 

SECRET_KEY = "key"
ALGORITHM = "HS256"

def get_current_user(token: str = Header(...)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return payload
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid token")

def validate_event(event: str):
    # Placeholder for event validation logic
    # You can implement your own logic based on the requirements
    if event not in ["event1", "event2"]:
        raise HTTPException(status_code=400, detail="Invalid event")

@app_events.post("/notify", methods=["POST"])
async def notify(
    message: dict,
    current_user: dict = Depends(get_current_user)
):
    try:
        # Check if the request is from the users server
        if current_user.get("source") == "users_server":
            # Notify the data server about the event
            response_data = requests.post(f"{data_server_url}/process_event", json=message).json()

            # Process the response and return it
            return {"message": "Event notification successful", "data_server_response": response_data}

        # Check if the request is from the data server
        elif current_user.get("source") == "data_server":
            # Validate the event from the data server
            validate_event(message.get("event"))

            # Perform the required action on the main server
            # Placeholder for the action logic (replace with your actual logic)
            response_main = {"message": "Action on main server performed successfully"}

            # Return the response
            return {"message": "Event notification successful", "main_server_response": response_main}

        else:
            # If the source is not recognized, raise an error
            raise HTTPException(status_code=400, detail="Invalid source")

    except requests.RequestException as e:
        # Handle any exceptions that might occur during the requests
        raise HTTPException(status_code=500, detail=f"Error notifying servers: {str(e)}")



@app_events.post("/notify", methods=["POST"])
async def notify(message: dict):
    try:
        response_data = requests.post(f"{data_server_url}/process_event", json=message).json()
        response_users = requests.post(f"{users_server_url}/notify_users", json=message).json()

        # Process responses and return a combined response
        return {
            "message": "Event notification successful",
            "data_server_response": response_data,
            "users_server_response": response_users,
        }
    except requests.RequestException as e:
        # Handle any exceptions that might occur during the requests
        raise HTTPException(status_code=500, detail=f"Error notifying servers: {str(e)}")

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app_events, host="0.0.0.0", port=5001)


