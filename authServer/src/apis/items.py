from fastapi import APIRouter

from pydantic import BaseModel

from src.prisma import prisma

router = APIRouter()


@router.get("/items")
async def get_items():
    return await prisma.item.find_many()


@router.get("/items/{item_id}")
async def get_item(item_id: int):
    return await prisma.item.find_unique(where={
        'id': item_id,
    })


class CreateItem(BaseModel):
    name: str

@router.post("/create")
async def create_item(createItem: CreateItem):
    created = await prisma.item.create(data={
        "name": createItem.name
    })
    return created


