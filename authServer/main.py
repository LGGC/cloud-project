from contextlib import asynccontextmanager
from fastapi import FastAPI
from fastapi.testclient import TestClient
from src.prisma import prisma
from src.apis import apis

@asynccontextmanager
async def lifespan(app: FastAPI):
    await prisma.connect()
    yield
    await prisma.disconnect()

app = FastAPI(lifespan=lifespan)

app.include_router(apis, prefix="/apis")



@app.get("/")
def read_root():
    return {"version": "1.0.0"}