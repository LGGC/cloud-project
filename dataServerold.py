from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
app_data = FastAPI()



# Allow all origins for the sake of this example (in a real-world scenario, limit this to your frontend's domain)
app_data.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://mainip:port",
        "http://eventsip:port",
    ],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"], #put -> update
    allow_headers=["*"]
)

# Placeholder for a database
data_db = {
    "uploads": [],
    "upgrades": [],
    "deletes": [],
    "downloads": [],
}

@app_data.post("/process_upload", methods=["POST"])
async def process_upload(message: dict):
    data_db["uploads"].append(message)
    return {"message": "Upload processed successfully"}

@app_data.put("/process_upgrade", methods=["PUT"])
async def process_upgrade(message: dict):
    data_db["upgrades"].append(message)
    return {"message": "Upgrade processed successfully"}

@app_data.delete("/process_delete", methods=["DELETE"])
async def process_delete(message: dict):
    data_db["deletes"].append(message)
    return {"message": "Delete processed successfully"}

@app_data.get("/process_download", methods=["GET"])
async def process_download(criteria: str):
    downloaded_data = data_db.get("downloads", [])
    return {"message": "Download processed successfully", "data": downloaded_data}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app_data, host="0.0.0.0", port=5002)

