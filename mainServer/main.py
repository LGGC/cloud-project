from fastapi import FastAPI
from src.apis import apis


app = FastAPI()
app.include_router(apis, prefix="/apis")



@app.get("/")
def read_root():
    return {"version": "1.0.0"}