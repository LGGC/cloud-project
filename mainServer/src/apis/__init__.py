from fastapi import APIRouter
from src.apis.data import router as dataRouter
from src.apis.auth import router as authRouter



apis = APIRouter()

apis.include_router(dataRouter)
apis.include_router(authRouter)

__all__ = ["apis"]