import requests


users_server_url = "http://users-server-ip:5003" 
main_server_url = "http://main-server-ip:5000" 


# authenticate user and get a token from the users server
def authenticate_user(username, password):
    try:
        credentials = {"username": username, "password": password}
        response = requests.post(f"{users_server_url}/authenticate", json=credentials)

        # Check if authentication was successful (status code 200)
        if response.status_code == 200:
            token = response.json().get("token")
            print("Authentication successful. Token:", token)
            return token
        else:
            # If authentication failed, print the error message
            response.raise_for_status()
    except requests.RequestException as e:
        # Handle any exceptions that might occur during the request
        print(f"Error authenticating user: {str(e)}")


def send_request_to_main_server(token, request_data):
    try:
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.post(f"{main_server_url}/process_request", json=request_data, headers=headers)

        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            print("Request to main server successful. Response:", response.json())
        else:
            # If the request failed, print the error message
            response.raise_for_status()
    except requests.RequestException as e:
        # Handle any exceptions that might occur during the request
        print(f"Error sending request to main server: {str(e)}")



# Example usage:
# Replace 'your_username' and 'your_password' with actual credentials
username = "your_username"
password = "your_password"

# Authenticate user and get a token
token = authenticate_user(username, password)

if token:
    # Send a request to the main server with the obtained token
    request_data = {"action": "upload", "file": "example.txt"}
    send_request_to_main_server(token, request_data)
