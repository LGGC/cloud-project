from fastapi import APIRouter
from src.apis.files import router as fileRouter

apis = APIRouter()

apis.include_router(fileRouter)

__all__ = ["apis"]