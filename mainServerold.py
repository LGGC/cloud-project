from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
import requests

app_main = FastAPI()
data_server_url = "http://data-server-ip:5002"


app_main.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

@app_main.get("/download", methods=["GET"])
async def download():
    try:
        response = requests.post(f"{data_server_url}/process_download", json={'message': 'File downloaded'})
        if response.status_code == 200:
            data = response.json()
            return JSONResponse(content={"message": "Data fetched successfully", "data": data})
        else:
            response.raise_for_status()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=f"Error fetching data from the data server: {str(e)}")


@app_main.post("/upload", methods=["POST"])
async def upload():
    try:
        response = requests.post(f"{data_server_url}/process_upload", json={'message': 'File uploaded'})

        if response.status_code == 200:
            return JSONResponse(content={"message": "File uploaded and processed successfully"})
        else:
            response.raise_for_status()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=f"Error processing upload on the data server: {str(e)}")

@app_main.put("/upgrade", methods=["PUT"])
async def upgrade():
    try:
        response = requests.put(f"{data_server_url}/process_upgrade", json={'message': 'File upgraded'})

        if response.status_code == 200:
            return JSONResponse(content={"message": "File upgraded and processed successfully"})
        else:
            response.raise_for_status()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=f"Error processing upgrade on the data server: {str(e)}")

@app_main.delete("/delete", methods=["DELETE"])
async def delete():
    try:
        response = requests.delete(f"{data_server_url}/process_delete", json={'message': 'File deleted'})

        if response.status_code == 200:
            return JSONResponse(content={"message": "File deleted and processed successfully"})
        else:
            response.raise_for_status()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=f"Error processing delete on the data server: {str(e)}")


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app_main, host="127.0.0.1", port=5001)
